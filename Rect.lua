
local vector = require "hump.vector"

-- NOTE: This currently depends on hump.vector, should probably make my own derivative
--       to future proof stuff =P
-- Should also put license stuff and such
-- Look into what the "best" way is to create new objects of this type (see
-- hump vector and hump vector-light)
-- NOTE: is currently not fully implemented, stuff will be added as needed
-- NOTE: I think I'll have this as a more "loose" library, which only depends
--       on having to vectors: dims and pos. All functions take arguments as such:
--       f(x, y, w, h), it then tests wether x is a rect, x, y are two vectors
--       or x, y, w, h are numbers.

Rect = {}

function Rect:new (obj)
  obj = obj or {}

  -- TODO: look into how to make this "better"...
  setmetatable(obj, self)
  self.__index = self

  obj.pos = obj.pos or vector(0, 0)
  obj.dims = obj.dims or vector(1, 1)

  return obj
end

local function isRect (r) -- TODO: add checks for x, y, w, h?
  return type(r) == "table" and vector.isvector(r.pos) and vector.isvector(r.dims)
end
Rect.isRect = isRect



-- Position and dimension getters: --------------------------------
function Rect.center (a, b, w, h)
  if isRect(a) then
    return a.pos + a.dims / 2
  elseif vector.isvector(a) and vector.isvector(b) then
    return a + b / 2
  else
    return vector(x + w / 2, y + h / 2)
  end
end

-- TODO: generalize all getters below to all "rects"
-- corner positions ---------------
function Rect.topRight(a, b, w, h)
  if isRect(a) then
    return a.pos
  end
end

function Rect.topLeft (a, b, w, h)
  if isRect(a) then
    return a.pos + vector(a.dims.x, 0)
  end
end

function Rect.bottomRight (a, b, w, h)
  if isRect(a) then
    return a.pos + vector(0, a.dims.y)
  end
end

function Rect.bottomLeft (a, b, w, h)
  if isRect(a) then
    return a.pos + a.dims
  end
end


-- side values --------------
function Rect.right(a, b, w, h)
  if isRect(a) then
    return a.pos.x + a.dims.x
  end
end

function Rect.bottom(a, b, w, h)
  if isRect(a) then
    return a.pos.y + a.dims.y
  end
end

function Rect.left (a, b, w, h)
  if isRect(a) then
    return a.pos.x
  end
end

function Rect.top (a, b, w, h)
  if isRect(a) then
    return a.pos.y
  end
end


-- Side segments -------------
-- function Rect.topSeg (a, b, w, h)
--   if isRect(a) then
--     return Rect.topRight(a) - Rect.topLeft(a)
--   end
-- end
--
-- function Rect.leftSeg (a, b, w, h)
--   if isRect(a) then
--     return Rect.bottomLeft(a) - Rect.topLeft(a)
--   end
-- end
--
-- function Rect.rightSeg (a, b, w, h)
--   if isRect(a) then
--     return Rect.bottomRight(a) - Rect.topRight(a)
--   end
-- end
--
-- function Rect.bottomSeg (a, b, w, h)
--   if isRect(a) then
--     return Rect.bottomRight(a) - Rect.bottomLeft(a)
--   end
-- end


function Rect.diagonalLength(a, b, w, h)
  if isRect(a) then
    return wmath.dist(a.pos.x, a.pos.y, a.dims.x, a.dims.y)

  elseif vector.isvector(a) and vector.isvector(b) then
    return wmath.dist(a.x, a.y, b.x, b.y)

  else
    return wmath.dist(a, b, w, h)

  end
end


-- Intersection with a line segment
-- returns nil if no intersections happens,
-- or the coordinates of one or two intersection points (ix1,iy1, ix2,iy2).
-- If a segment's beginning or end is inside the given box,
-- it is considered an intersection, too.
-- Credit to: https://love2d.org/forums/viewtopic.php?f=5&t=11752
-- (Modified to optionally take a rect and two vectors)
function Rect.segmentIntersection(l, t, w, h, x1, y1, x2, y2)
  if isRect(l) then
    if vector.isvector(t) and vector.isvector(w) then
      x1, y1, x2, y2 = t.x, t.y, w.x, w.y
    end
    l, t, w, h = l.pos.x, l.pos.y, l.dims.x, l.dims.y
  end

  local dx, dy = x2 - x1, y2 - y1

  local t0, t1 = 0, 1
  local p, q, r

  for side = 1, 4 do
    if side == 1 then p, q = -dx, x1 - l
    elseif side == 2 then p, q = dx, l + w - x1
    elseif side == 3 then p, q = -dy, y1 - t
    else p, q = dy, t + h - y1
    end

    if p == 0 then
      if q < 0 then return nil end -- Segment is parallel and outside the bbox
    else
      r = q / p
      if p < 0 then
        if r > t1 then return nil
        elseif r > t0 then t0 = r
        end
      else -- p > 0
        if r < t0 then return nil
        elseif r < t1 then t1 = r
        end
      end
    end
  end

  local ix1, iy1, ix2, iy2 = x1 + t0 * dx, y1 + t0 * dy,
  x1 + t1 * dx, y1 + t1 * dy

  if ix1 == ix2 and iy1 == iy2 then return ix1, iy1 end
  return ix1, iy1, ix2, iy2
end

-- Self made version of above, more useful for specific situations
-- NOTE: all arguments will be floored so as to avoid floating point error stuff
function Rect.boxSegmentIntersections(rect, p1, p2)
  local rect, p1, p2 = Rect:new(rect), p1:clone(), p2:clone()

  rect.pos.x , rect.pos.y  = math.floor(rect.pos.x ), math.floor(rect.pos.y )
  rect.dims.x, rect.dims.y = math.floor(rect.dims.x), math.floor(rect.dims.y)

  p1.x, p1.y = math.floor(p1.x), math.floor(p1.y)
  p2.x, p2.y = math.floor(p2.x), math.floor(p2.y)

  local ix1, iy1, ix2, iy2 = false, 0, false, 0

  for side = 1, 4 do
    if     side == 1 then rsegx1, rsegy1, rsegx2, rsegy2 = rect.pos.x              , rect.pos.y              , rect.pos.x + rect.dims.x, rect.pos.y
    elseif side == 2 then rsegx1, rsegy1, rsegx2, rsegy2 = rect.pos.x              , rect.pos.y              , rect.pos.x              , rect.pos.y + rect.dims.y
    elseif side == 3 then rsegx1, rsegy1, rsegx2, rsegy2 = rect.pos.x + rect.dims.x, rect.pos.y              , rect.pos.x + rect.dims.x, rect.pos.y + rect.dims.y
    elseif side == 4 then rsegx1, rsegy1, rsegx2, rsegy2 = rect.pos.x              , rect.pos.y + rect.dims.y, rect.pos.x + rect.dims.x, rect.pos.y + rect.dims.y
    end

    local x, y = wutil.findIntersect(p1.x, p1.y, p2.x, p2.y, rsegx1, rsegy1, rsegx2, rsegy2, true, true)
    if x then
      if ix1 then
        ix2, iy2 = x, y
      else
        ix1, iy1 = x, y
      end
    end
  end

  return ix1, iy1, ix2, iy2
end


function Rect.draw (r, opts)
  opts.mode = opts.mode or "fill"
  if opts.color then love.graphics.setColor(opts.color) end

  local pos
  if opts.centered then
    pos = r.pos - r.dims / 2
  else
    pos = r.pos
  end

  love.graphics.rectangle(opts.mode, pos.x, pos.y, r.dims.x, r.dims.y)
end

return Rect
