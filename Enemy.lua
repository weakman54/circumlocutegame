
Enemy = {}
wlove.attachEmptyLoveCallbacks(Enemy)

function Enemy:new(obj)
  obj = obj or {}

  setmetatable(obj, self)
  self.__index = self


  obj.pos = obj.pos or vector(0, 0)
  obj.dims = obj.dims or vector(50, 50)

  obj.speed = 100 -- (px/s)

  obj.color = obj.color or {255, 0, 0}

  obj.colRect = HC.rectangle(obj.pos.x, obj.pos.y, obj.dims.x, obj.dims.y)

  obj.target = obj.target or {pos = vector(0, 0)} -- obj with pos vector
  obj.stopDistance = 1 -- (px)


  return obj
end


function Enemy:setPosition (x, y)
  if vector.isvector(x) then
    self.pos = x
  else
    self.pos.x = x
    self.pos.y = y
  end

  self.colRect:moveTo(self.pos.x, self.pos.y)
end


function Enemy:findPlayfieldIntersect()
  -- Ignoring the cases where both points are the same, and where both points are outside of the rect
  -- it will never happen
  local x1, y1, x2, y2 = Playfield:boxSegmentIntersections(self.pos, self.target.pos)
  -- print(x1, y1, x2, y2)
  if x1 then
    self.intersectPoint = vector(x1, y1)
  elseif x2 then
    self.intersectPoint = vector(x2, y2)
  else
    -- print(":", x1, y1, x2, y2)
    -- print("#", self.pos, self.target.pos)
    self.intersectPoint = nil
  end

  self.DBG_I1, self.DBG_I2 = vector(x1, y1), vector(x2, y2)
end


function Enemy:update(dt)
  self:findPlayfieldIntersect()

  self.targetVec = self.target.pos - self.pos

  if self.targetVec:len() > self.stopDistance then
    self:setPosition(wvector.changedBy(self.pos, self.targetVec:normalized(), self.speed, dt))
  end
end

function Enemy:draw()
  Rect.draw(self, {color = self.color, centered = true})

  if self.intersectPoint then
    love.graphics.setColor(255, 255, 255)
    love.graphics.circle("line", self.intersectPoint.x, self.intersectPoint.y, 50)
  end
end

return Enemy
