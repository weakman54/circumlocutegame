
SOptions = {}
SOptions.name = "Options"


-- GUI functions ----------------------------------
function SOptions:GUIMain ()
  GUILayoutInitial()
  GUITitleScreen("Options")

  for _, button in ipairs(self.buttons) do
    GUIButton(button.label, self.name, button.callback, unpack(button.cbargs))
  end
end


-- Main functions --------------------------------
function SOptions:init()
  self.buttons = {
    {label = "Toggle music", callback = loveSourceToggle, cbargs = {bgm}},
    {label = "Back", callback = gamestate.pop, cbargs = {}}
  }
end

function SOptions:enter (from)
  self.from = from
end

function SOptions:update (dt)
  if input:pressed("back") then
    gamestate.pop()
  end

  self:GUIMain()
end

function SOptions:draw()
  suit.draw()
end


return SOptions
