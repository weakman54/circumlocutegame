
Player = {}
wlove.attachEmptyLoveCallbacks(Player) -- bit of extra "work" since I redefine functions but eh

function Player:new(obj)
  obj = obj or {}

  setmetatable(obj, self)
  self.__index = self


  obj.pos = obj.pos or vector(0, 0)
  obj.dims = obj.dims or vector(50, 50)

  obj.speed = obj.speed or 200 -- px/s (?)

  obj.color = obj.color or {255, 255, 255}

  obj.colRect = HC.rectangle(obj.pos.x, obj.pos.y, obj.dims.x, obj.dims.y)


  return obj
end


function Player:setPosition (x, y)
  if vector.isvector(x) then
    self.pos = x
  else
    self.pos.x = x
    self.pos.y = y
  end

  self.colRect:moveTo(self.pos.x, self.pos.y)
end


function Player:move(dt)
  local dir = wvector.moveDirFromKeys(
    input:down("moveUp"),
    input:down("moveDown"),
    input:down("moveLeft"),
    input:down("moveRight")
  )
  self:setPosition(wvector.changedBy(self.pos, dir:normalized(), self.speed, dt))
end


function Player:constrainToPlayfield()
  if self.pos.x < Playfield.pos.x then
    self:setPosition(Playfield.pos.x, self.pos.y)

  elseif self.pos.x > Playfield:right() then
    self:setPosition(Playfield:right(), self.pos.y)
  end

  if self.pos.y < Playfield.pos.y then
    self:setPosition(self.pos.x, Playfield.pos.y)

  elseif self.pos.y > Playfield:bottom() then
    self:setPosition(self.pos.x, Playfield:bottom())
  end
end


function Player:update (dt)
  self:move(dt)
  self:constrainToPlayfield()
end

function Player:draw ()
  Rect.draw(self, {color = self.color, centered = true})
end

return Player
