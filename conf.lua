
require "properties"

function love.conf (t)
  t.console = true

  t.window.title = "Circumlocute"
  t.window.icon = "icon.png"

  t.window.width = WIN_MIN_SIZE
  t.window.height = WIN_MIN_SIZE

  t.window.resizable = true

  t.window.minwidth = WIN_MIN_SIZE
  t.window.minheight = WIN_MIN_SIZE
end
