
-- Playfield
local size = Playfield_Size
Playfield = Rect:new{pos = vector(Playfield_Padding, Playfield_Padding), dims = vector(size, size)}

function Playfield:centerOnScreen()
  local dw = width() - WIN_MIN_SIZE
  local dh = height() - WIN_MIN_SIZE

  self.pos = vector(dw, dh) / 2
end

function Playfield:draw (args)
  Rect.draw(self, args)

  love.graphics.setColor(0, 0, 0) -- TODO: define colors of all elements

  love.graphics.rectangle("fill", 0, 0, width(), self:top() - 1)
  love.graphics.rectangle("fill", 0, 0, self:left() - 1, height()) -- NOTE: height is too "long"
  love.graphics.rectangle("fill", self:right() + 1, 0, width(), height())
  love.graphics.rectangle("fill", 0, self:bottom() + 1, width(), height() - self:bottom())
end
