
suit = require "SUIT"
HC = require "HC"
gamestate = require "hump.gamestate"
vector = require "hump.vector"
Input = require "boipushy.Input"

require "Rect"

util = require "WUtil.wutil"
wlove = require "WUtil.wlove"
wmath = require "WUtil.wmath"
wvector = require "WUtil.wvector"

require "properties"

require "Playfield"
require "GUIFunctions"
require "Keybindings"


-- Gamestates
require "SMain"
require "SOptions"
require "SGame"
require "SPause"

-- print(SMain)
-- print(SOptions)
-- print(SGame)
-- print(SPause)


-- Functions (and aliases): -------------
width = love.graphics.getWidth
height = love.graphics.getHeight


-- TODO: make proper utility function of this in a love 2d util thing
function loveSourceToggle (source)
  if source:isPlaying() then source:stop()
  else source:play() end
end


function bindInputs()
  for action, keys in pairs(keybinds) do
    for _, key in ipairs(keys) do
      input:bind(key, action)
    end
  end
end


-- "Main" ------------------------------------
function love.load(arg)
  -- print(_VERSION)
  math.randomseed( os.time() )

  gamestate.registerEvents() -- modifies all of loves callbacks to include calls to gamestates callbacks!
  gamestate.switch(SMain)

  bgm = love.audio.newSource("Assets/Music/circumlocute1.wav", "stream")
  bgm:setLooping(true)
  if Play_Music_On_Start then bgm:play() end

  defaultFont = love.graphics.newFont(fontfname, 14)
  mediumFont = love.graphics.newFont(fontfname, 20)
  largeFont = love.graphics.newFont(fontfname, 50)
  love.graphics.setFont(defaultFont)


  input = Input()
  bindInputs()
end

function love.update(dt)
end

function love.draw()
end

function love.resize(w, h)
  -- print(w, h)
  Playfield:centerOnScreen()
end
