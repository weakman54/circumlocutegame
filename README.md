# Circumlocute #
**A game about avoiding things**

# Controls
Currently only WASD and arrow-keys are supported.

Plans exist to add controller-support, remap-able keys (for controller too!), mouse-movement, touch support, accelerometer (yes, I want to port this to phones too), and more(?)

# How to build?
If you can read this, you are probably reading the readme from the git repo, which means that you are going to have to either install Love2D and run it with that yourself.

git clone --recursive, to get all the submodules

You can also ask me for a download, but I'm not too willing to create proper builds in this stage of the development, since I haven't set my workflow up proper yet. But if you ask nicely, I can probably accommodate.

# Contact
Enki Waller (weakman54) - enkiigm@gmail.com

None for the music composer yet unfortunately
