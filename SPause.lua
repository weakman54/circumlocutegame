
SPause = {}
SPause.name = "Pause"

-- GUI functions ----------------------------------
function SPause:GUIMain ()
  GUILayoutInitial()
  GUITitleScreen("Game Paused")

  for _, button in ipairs(self.buttons) do
    GUIButton(button.label, self.name, button.callback, unpack(button.cbargs))
  end
end


-- Main functions ---------------------------------
function SPause:init()
  self.buttons = {
    {label = "Resume", callback = gamestate.pop, cbargs = {}},
    {label = "Restart", callback = gamestate.pop, cbargs = {SGame}},
    {label = "Options", callback = gamestate.push, cbargs = {SOptions}},
    {label = "Main Menu", callback = gamestate.pop, cbargs = {SMain}},
    {label = "Exit to desktop", callback = love.event.quit, cbargs = {}},
  }
end

function SPause:enter (from)
  -- print("Pause enter")
  self.from = from
end

function SPause:update (dt)
  if input:pressed("back") then
    gamestate.pop()
  end

  self:GUIMain()
end

function SPause:draw()
  -- draw previous screen
  self.from:draw(true)
  -- overlay with pause message
  love.graphics.setColor(0, 0, 0, 100)
  love.graphics.rectangle('fill', 0, 0, width(), height())
  suit.draw()
end


return SPause
