
fontfname = "Assets/Font/Courier-Prime-Sans/Courier Prime Sans Bold.ttf"


Playfield_Size = 720
WIN_MIN_SIZE = Playfield_Size

Playfield_Padding = 0

SUIT_Cell_Width = WIN_MIN_SIZE / 5
SUIT_Small_Cell_Width = SUIT_Cell_Width / 2

SUIT_Cell_Heigth = WIN_MIN_SIZE / 20
SUIT_Small_Cell_Heigth = SUIT_Cell_Heigth / 2


Play_Music_On_Start = false


-- Game stuff ----------------
Player_Size =  WIN_MIN_SIZE/15

Enemy_Size = 9*Player_Size / 10
Enemy_Spawn_Distance = Playfield_Size*2
