
keybinds = {
  back = {"escape"},
  pause = {"escape"},

  moveUp = {"w", "up"},
  moveRight = {"d", "right"},
  moveDown = {"s", "down"},
  moveLeft = {"a", "left"},
}

-- return keybindings
