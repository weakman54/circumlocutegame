
local Enemy = require "Enemy"
local Player = require "Player"


-- Debug --------------
DBG_Disable_Collisions = false
DBG_Override_Spawn_Time = false


DBG_Draw = false

DBG_Lines = {}
DBG_Draw_Spawn_Lines = false
DBG_Draw_Enemy_Intersect_Lines = true


--------------------------------------------------------------------------------
-- SGame: ----------------------------------------------------------------------
SGame = {}
SGame.name = "Game"

-- GUI functions: ------------------
function GUIScoreLabel(score)
  suit.Label(string.format("Score: %06.2f", score), {font = mediumFont, align = "left", valign = "center"}, suit.layout:row(SUIT_Cell_Width * 2, SUIT_Cell_Heigth)) -- TODO: still kinda bad calc for score placement
end

function SGame:GUIMain()
  GUILayoutInitial()

  if self.isGameOver then
    GUITitleScreen("Game Over")
    suit.layout:up() -- Reset spacing for score label
  end

  GUIScoreLabel(wmath.round(self.score, 2))

  if self.isGameOver then
    for _, button in ipairs(self.buttons) do
      GUIButton(button.label, self.name, button.callback, unpack(button.cbargs))
    end
  end
end


-- Enemy stuff -------------
function SGame:spawnEnemy ()
  local randVec = vector.randomDirection((Playfield:diagonalLength() / 2) + 70) -- TODO: fix magic number for diagonal of enemy diagonal
  local spawnVec = Playfield:center() + randVec
  table.insert(self.entities, Enemy:new{pos = spawnVec, target = self.player, dims = vector(Enemy_Size, Enemy_Size)})

  if DBG_Draw_Spawn_Lines then
    table.insert(DBG_Lines, {Playfield:center(), spawnVec})
  end
end

function SGame:checkSpawnEnemies (dt)
  self.enemyTimer = self.enemyTimer + dt

  if self.enemyTimer >= self.Enemy_Target_Time then
    self:spawnEnemy()
    self.enemyTimer = self.enemyTimer - self.Enemy_Target_Time
  end
end


-- Other stuff ----------------------
function SGame:gameOver ()
  self.doUpdate = false
  self.isGameOver = true
end

function SGame:checkCollisions ()
  if DBG_Disable_Collisions then
    return
  end
  for shape, delta in pairs(HC.collisions(self.player.colRect)) do
    self:gameOver()
  end
end


function SGame:clearCollisionShapes()
  for _, entity in ipairs(self.entities) do
    HC.remove(entity.colRect)
  end
end


function SGame:updateScore (dt)
  self.score = self.score + dt
end


function SGame:increaseSpeed(dt)
  for _, entity in ipairs(self.entities) do
    entity.speed = entity.speed + (entity.speed * self.speedScaleFactor * dt)
  end
end


function SGame:pause ()
  gamestate.push(SPause)
end


-- Main functions -------------------------------------
function SGame:init()
  self.buttons = {
    {label = "Play again", callback = gamestate.switch, cbargs = {SGame}},
    {label = "Main Menu", callback = gamestate.switch, cbargs = {SMain}},
    {label = "Exit to desktop", callback = love.event.quit, cbargs = {}},
  }
end

function SGame:enter (from) -- Use this as initializer to ensure proper behaviour regardless of when state is entered
  -- print("Enter Game")
  self.entities = {}

  self.doUpdate = true
  self.isGameOver = false

  self.enemyTimer = 0 -- seconds
  self.Enemy_Target_Time = DBG_Override_Spawn_Time or 3 -- seconds

  self.player = Player:new{pos = Playfield:center()}
  table.insert(DBG_Lines, {vector(0, 0), Playfield:center()})

  table.insert(self.entities, self.player)

  self.score = 0

  self.speedScaleFactor = 0.01;


  self:spawnEnemy() -- Start by spawing an enemy immediately
end

function SGame:leave()
  -- print("Leave Game")
  self:clearCollisionShapes()
end

function SGame:resume (_, switchTo)
  -- print("Resume Game")
  -- print(switchTo)

  if switchTo then
    gamestate.switch(switchTo)
  end
end

function SGame:update(dt)
  if self.doUpdate then
    self:checkSpawnEnemies(dt)
    self:checkCollisions()

    self:updateScore(dt)

    self:increaseSpeed(dt)

    for _, entity in ipairs(self.entities) do
      entity:update(dt)
    end

    if input:pressed("pause") then
      self:pause()
    end
  end

  self:GUIMain()
end


function SGame:draw(noGUI)
  for _, entity in ipairs(self.entities) do
    entity:draw()
  end

  Playfield:draw{mode = "line", color = {255, 255, 255}}

  if not noGUI then
    suit.draw()
  end

  if DBG_Draw then
    love.graphics.setColor(255, 0, 255)
    for _, line in ipairs(DBG_Lines) do
      love.graphics.line(line[1].x, line[1].y, line[2].x, line[2].y)
    end

    if DBG_Draw_Enemy_Intersect_Lines then
      for _, entity in ipairs(self.entities) do
        if entity ~= self.player then
          love.graphics.line(entity.DBG_I1.x, entity.DBG_I1.y, entity.DBG_I2.x, entity.DBG_I2.y)
        end
      end
    end
  end
end


function SGame:resize(w, h)
  self:GUIMain()
end


function SGame:focus(focused)
  if not focused then
    self:pause()
  end
end


return SGame
