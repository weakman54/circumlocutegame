
function GUILayoutInitial ()
  suit.layout:reset(Playfield.pos.x, Playfield.pos.y, 10)
end


function GUITitleScreen(title)
  if type(title) ~= "string" then error("please supply title") end

  suit.layout:col(SUIT_Small_Cell_Width, SUIT_Small_Cell_Heigth)
  suit.layout:col()
  suit.layout:row()

  suit.Label(title, {font = largeFont, align = "left"}, suit.layout:row(2 * WIN_MIN_SIZE / 3, SUIT_Cell_Heigth))

  suit.layout:row(SUIT_Cell_Width / 3, SUIT_Cell_Heigth)

  suit.layout:col(SUIT_Cell_Width, SUIT_Cell_Heigth)
end


local _buttonLabels = {}
function GUIButton (label, id, callback, ...)
  label = label or "No Label"
  id = label .. "_" .. id

  if suit.Button(label, {id = id}, suit.layout:row(SUIT_Cell_Width, SUIT_Cell_Heigth)).hit then
    callback(...)
  end
end
