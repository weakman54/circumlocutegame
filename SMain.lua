
SMain = {}
SMain.name = "Main"



-- GUI functions ----------------------------------
function SMain:GUIMain ()
  GUILayoutInitial()
  GUITitleScreen("Circumlocute")

  for _, button in ipairs(self.buttons) do
    GUIButton(button.label, self.name, button.callback, unpack(button.cbargs))
  end
end


-- Main functions -------------------------------------------------
function SMain:init()
  self.buttons = {
    {label = "Play", callback = gamestate.switch, cbargs = {SGame}},
    {label = "Options", callback = gamestate.push, cbargs = {SOptions}},
    {label = "Exit Game", callback = love.event.quit, cbargs = {}},
  }
end

function SMain:update (dt)
  self:GUIMain()
end

function SMain:draw()
  suit.draw()
end

return SMain
